%\section{Methods}
%    \fontsize{5mm}{8mm}\selectfont
%    \input{sec_methods}

\subsection{Notations and Problem Statement}
We consider a set of $N$ bi-allelic SNPs, where each SNP has three genotype values encoded
as 0, 1 or 2 according to the number of copies of the minor allele present. We assume
we are given the genotype values for all SNPs for the same subset of individuals from a large
population.  Each individual is classified into either a case group  {\em (cases)}
or a control group {\em (controls)} according to
whether they do or do not posses a given phenotype respectively. The total number of cases and controls
are denoted as $n_1$ and $n_0$ respectively.  The phenotype value $y_k$
for  individual $k$ is  encoded as 0 if that individual belongs to controls and as 1 if that
individual belongs cases.

For two-way epistatic interactions consider a pair of SNPs, at loci $a$ and $b$
respectively.
Let $x_{ak}$ denote the genotype value for individual $k$ at locus $a$ and likewise
let $x_{bk}$ denote the genotype for the same individual at locus $b$.
The counts of all genotype values for the pair of SNPs can be organised into two
  $3\times3$   contingency tables, one for    controls and one for   cases.
%For this purpose, we denote first $S_{li}^c = \{k : k \in I_c , x_{lk} = i\}$
%the subset of indices for
%individuals with phenotype $c\in \{0, 1\}$ and allele $i \in \{0, 1, 2\}$ at the locus $l \in \{a,b\}$.
We define $n_{ij}^0$ as the number
of controls with genotype $i$ at locus $a$ and $j$ at locus
$b$. Likewise $n_{ij}^1$ denotes counts for the same genotypes in cases.
Each of the nine different combinations of allele
$(i, j)$   gives rise to an individual cell in each of the two contingency tables
$[n^c_{ij}]_{0\leq i, j\leq 2}$ for $c = 0, 1$.
The marginal counts for each table are denoted by
\begin{eqnarray*}
    n_{i*}^{c} &:=& \big|\{k : k \in I_c , x_{ak} = i\}\big| = \sum_{j = 0 }^2n^c_{ij},\\
    n_{*j}^{c} &:=& \big|\{k : k \in I_c , x_{bk} = j\}\big| = \sum_{i = 0 }^2n^c_{ij},
 \end{eqnarray*}
where the phenotype $c = {0, 1}$, respectively.
In Figure~\ref{fig:notation}, we illustrate
the notation of a contingency table for a given SNP pair, $a$ and $b$ and for the two
individual constituent SNPs.

\subsection{Supported Statistics}

This section   describes  selected sample    methods implemented   in this paper
using our functional interface.

\subsubsection{SHEsisEPI}
\cite{Hu10} developed an odds-ratio ($OR$) based
method called SHEsisEpi that evaluates separately for cases and controls the odds-ratios for every genotype
combination of a given SNP pair using penetrance collected for that
particular genotype combination. Epistatic interactions are detected if at least one genotype
combination reports its $EOR:=OR_{Case}/OR_{Control}$ significantly deviating from one.

We have applied our best efforts to implement SHEsisEPI as published in \cite{Hu10}.
 However, a few details are unclear. In particular, the variance used in computation of the $Z$
 test is not explicitly defined. Hence we have assumed that the algorithm
 is similar to Fast Epistasis in PLINK, except it is applied to nine contingency tables of dimension $2\times 2$
 obtained from the complete $3 \times 3$ contingency table for a pair of SNPs. Moreover, the
 method appears to be sensitive to cells with 0 counts, hence we have added a correction of 0.5
 to every cell, prior to calculating the Z score for each $2 \times 2$ table.
 One complete table $[\alpha_{ij}]$ is used in PLINK
 (see Eqns.~(\ref{eq:def:alpha11}- \ref{eq:def:alpha22}).
 Although source code is available for this algorithm,  we were unable to run it successfully with multiple attempts.
 %Therefore, scores resulting from our implementation of this method have not been verified.

\subsubsection{BOOST}

Logistic regression $\log(p/(1-p))=\alpha+\beta X_{B}+\gamma X_{C}+iX_{B}X_{C}$ can
be used to model the association between disease risk $p$ and the genotypes $X_B$
and $X_C$ from two SNPs, respectively~\citep{Cordell09}. According to Fisher's
definition of statistical epistasis, the deviation of the full model from the
additive model of logistic regression, which is presented as a log-likelihood
ratio test (LRT), can be used to measure the interaction effects between two SNPs.
 %Mathematically, this tests the effect size of the term $iX_{B}X_{C}$.
The ``BOolean Operation-based Screening and Testing'' (BOOST) method proposed
by~\cite{Wan10} implemented this LRT of the interaction effects based on the
equivalent log-linear regression, which significantly improves computational
efficiency through modeling contingency tables in place of raw data. Later,
a GPU version of BOOST (GBOOST~\citep{Yung11}) was released to speed up
processing. We also implemented the BOOST method using the faster computation of
contingency tables in our GPU-based software. In our tests the results produced by GBOOST
and \GWISFI implementation only differ by magnitudes below $10^{-4}$ due to floating-point precision limits.


\subsubsection{Information Gain}

 The  information  gain  (IG)  has been proposed and used
   to capture higher order interactions \citep{Moore02,Chen09,Hu13}. IG  uses information
 theory to capture higher order interactions. In this
 case the phenotype and genotypes at candidate loci are taken as random variables.
 IG is based on the difference between the mutual information between the phenotype random variable
 and the two genotype random variables taken jointly, and the mutual
information between the phenotype random variable and each genotype random variable taken individually.

We have implemented IG according to the (standard) equations given in \cite{Moore02,Hu13};
the pseudo-code for our implementation is available
in the supplementary data online.
We have used  functions in R
to verify the correctness of our implementation for a few thousand selected cases of SNP-pairs in WTCCC T1D data.

