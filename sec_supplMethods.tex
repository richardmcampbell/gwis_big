

\subsection{Odds Ratio Based Method}
The difference of Odds Ratio (OR) between case and control is often used to test
pairwise interactions in large-scale case-control studies. The test relies on a
$Z$-score to assess the difference of log odds ratios between cases and controls for a given pair of SNPs.
In PLINK \citep{Purcell07,plink} the $Z$-score is computed as follows
\[
	Z =(\log(R^{1})-\log(R^{0}))/(\sqrt{(\mathrm{SE}(R^1)^2 + (\mathrm{SE}(R^0)^2})
\]
where $R^c$ for $c \in \{0,1\}$ is the odds ratio for the reduced contingency table given in equation
(\ref{eq:def:Z}) and $\mathrm{SE}(R^c)$ is the standard error for the log odds ratio given as the square root of the sum of the reciprocal cell counts.

SHEsisEPI built its method based on OR by calculating, for every genotype combination $ij$ in $3 \times 3$ contingency table a reduced $2 \times 2$ contingency table:
\[
\left( \begin{array}{cc}
			 n_{ij}^c & n_{\overline{i}j}^c \\
			 n_{i\overline{j}}^c & n_{\overline{i}\overline{j}}^c
	    \end{array} \right)
			 =
\left( \begin{array}{cc}
	         n_{ij}^{c} & \sum_{p\neq i} n_{pj}^c \\
	         \sum_{q\neq j} n_{iq}^c & \sum_{p\neq i q\neq j} n_{pq}^c
\end{array} \right),
\]

SHEsisEPI substitutes this contingency table for the one used by PLINK and uses the same expressions for $R^c$ and $\mathrm{SE}(R_c)$ given in section (\ref{sec:plat}).

For each of the nine combinations of $ij$ SHEsisEPI computes $EOR_{ij} := \frac{R^{1}_{ij}}{R^{0}_ij}$ and
then applies $Z$-score test to see if $EOR_{ij}$ deviates significantly from 1. Specifically,
$Z_{ij}:=\frac{\log(EOR_{ij})}{\sigma_{ij}}=\frac{\log(R^{1}_{ij})-\log(R^{0}_{ij})}{\sigma_{ij}}$, where $\sigma_{ij} = \sqrt{(\mathrm{SE}(R^1_{ij})^2 + (\mathrm{SE}(R^0_{ij})^2}$.
An epistatic interaction is considered to be present if any one of the nine $EOR_ij$ values deviates
significantly. Ranking of pairs can be done by sorting the smallest resultant $p$-value.
Since all elements are already in OE form, we can simply derive the Algorithm
\ref{algo:SHE}. In this case, for speed we use a pre-computed lookup
table of $p$-values for the $Z$ test, whose resolution will determine
the final accuracy of the test. Alternatively one may use the standard
library implementation of the error function for greater accuracy.

\IncMargin{1em}
\begin{algorithm}
\SetKwData{Left}{left}\SetKwData{This}{this}\SetKwData{Up}{up}
\SetKwFunction{Union}{Union}\SetKwFunction{FindCompress}{FindCompress}
\SetKwInOut{Input}{input}\SetKwInOut{Output}{output}
$S\leftarrow 0$\;
\For{$i\leftarrow 0$ \KwTo $2$}{
	\For{$c\leftarrow 0$ \KwTo $1$}{
		$n_{\overline{i}j}^c\leftarrow \sum_{p\neq i} n_{pj}^c$\;
		$n_{i\overline{j}}^c\leftarrow \sum_{q\neq j} n_{iq}^c$\;
		$n_{\overline{i}\overline{j}}^c\leftarrow \sum_{p\neq i q\neq j} n_{pq}^c$\;
	}
    \For{$j\leftarrow 0$ \KwTo $2$}{
    	\For{$c\leftarrow 0$ \KwTo $1$}{
        	$R^{c}_{ij}=\frac{n_{ij}^{c}*n_{\overline{i}\overline{j}}^{c}}{n_{i\overline{j}}^{c}*n_{\overline{i}j}^{c}}$\;
        	}
        $logEOR_{ij}\leftarrow \log(R^{1}_{ij})-\log(R^{0}_{ij})$\;

	    $\sigma\leftarrow\sqrt{\sum_{c=0}^1 1/n_{\overline{i}j}^c + 1/n_{i\overline{j}}^c + 1/n_{\overline{i}\overline{j}}^c +1/n_{ij}^c}$\;

        $Z_{ij}=logEOR_{ij}/\sigma$\;
        lookup $p$ value of largest score not exceeding $|Z_{ij}|$ in $p$-value table\;
        %$p=\mathrm{pvalue\_table}[Z_{ij}]$\;
        \If{$-log(p)>S$}{
            $S\leftarrow -log(p)$\;
        }
    }
}
\caption{Algorithm of EOR (SHEsisEPI) for a SNP pair via GWIS$_{FI}$.}\label{algo:SHE}
\end{algorithm}\DecMargin{1em}

\subsection{Information Gain Based Method}
Information Gain (IG) is used by \citep{Hu13} to capture high order interactions, and can be simplified to bivariate case. Here we treat the phenotype and genotypes at loci $a$ and $b$ as categorical random variables $Y$, $X_a$ and $X_b$. The information gain (IG) of $Y$ given the knowledge of genotypes $X_{a}$ and $X_{b}$ is defined in terms of the mutual information (MI) as follows:
\[
\mathrm{IG}(Y|X_{a},X_{b}):=\mathrm{MI}(X_{a},X_{b};Y)-\mathrm{MI}(X_{a};Y)-\mathrm{MI}(X_{b};Y)
\]
This formula subtracts the individual main effects of each SNP from their joint effect to give a gain in information. The mutual information between two discrete random variables  $\mathrm{MI}(X; Y) = H(Y) - H(Y|X)$ represents the reduction in uncertainty in $Y$ given knowledge of $X$, where $H$ is the information entropy $H(X) = -\sum_x P(X=x) \log P(X=x)$ or the amount of information required to describe the random variable $X$. The conditional entropy can be written in terms of the entropy of the joint distribution of $X$ and $Y$ as $H(Y|X) = H(X,Y) - H(X)$. If we use the contingency table counts to estimate the probability mass functions for $Y$, $X_a$ and $X_b$, we can render the expression for the information gain in OE form. The pseudocode in Algorithm \ref{algo:IG} illustrates how to compute the information gain from OE elements

\IncMargin{1em}
\begin{algorithm}
\SetKwData{Left}{left}\SetKwData{This}{this}\SetKwData{Up}{up}
\SetKwFunction{Union}{Union}\SetKwFunction{FindCompress}{FindCompress}
\SetKwInOut{Input}{input}\SetKwInOut{Output}{output}
$S\leftarrow 0$\;
$H(X_{a})\leftarrow 0$\;
$H(X_{b})\leftarrow 0$\;
$H(Y)\leftarrow 0$\;
$H(X_a,Y)\leftarrow 0$\;
$H(X_b,Y)\leftarrow 0$\;
$H(X_a,X_b,Y)\leftarrow 0$\;
$n\leftarrow n_0 + n_1$\;
\For{$i\leftarrow 0$ \KwTo $2$}{
	$n_{i*}\leftarrow n_{i*}^0 + n_{i*}^1$\;
	$n_{*i}\leftarrow n_{*i}^0 + n_{*i}^1$\;	
    $H(X_a)\leftarrow H(X_a) - \frac{n_{i*}}{n}(\log(n_{i*}) - \log(n))$\;
    $H(X_b)\leftarrow H(V_B) - \frac{n_{*i}}{n}(\log(n_{*i}) - \log(n))$\;
    \For{$j\leftarrow 0$ \KwTo $2$}{
    	$n_{ij}\leftarrow n_{ij}^0 + n_{ij}^1$\;
        $H(X_a,X_b)\leftarrow H(X_a,X_b)- \frac{n_{ij}}{n}(\log(n_{ij}) - \log(n))$\;
    }
}
\For{$c\leftarrow 0$ \KwTo $1$}{
    $H(Y)\leftarrow H(Y)- \frac{n_{c}}{n}(\log(n_{c}) - \log(n))$\;
    \For{$i\leftarrow 0$ \KwTo $2$}{
        $H(X_a,Y)\leftarrow H(X_a,Y)-\frac{n_{i*}^{c}}{n}(\log(n_{i*}^{c})-\log(n))$\;
        $H(X_b,Y)\leftarrow H(X_b,Y)-\frac{n_{*i}^{c}}{n}(\log(n_{*i}^{c})-\log(n))$\;
        \For{$j\leftarrow 0$ \KwTo $2$}{
            $H(X_a,X_b,Y)\leftarrow H(X_a,X_b,Y)-\frac{n_{ij}^{c}}{n}(\log(n_{ij}^{c})-\log(n))$\;
        }
    }
}
$S\leftarrow H(X_a,X_b)+H(X_a,Y)+H(X_b,Y)-H(Y)-H(X_a,X_b,Y)-H(X_a)-H(X_b)$\;
\caption{Algorithm of IG for a SNP pair via GWIS$_{FI}$.}\label{algo:IG}
\end{algorithm}\DecMargin{1em}

\subsection{BOOST}
BOOST/GBOOST method~\cite{Wan10} employs log-linear regression to implement the log-likelihood ratio test (LRT) for the interaction effect. The log-likelihood ratio between the saturated model
\begin{equation}\label{eq:boost-saturated}
\log{\mu_{ijc}}=\tau+\tau_{i}^{X_a}+\tau_{j}^{X_b}+\tau_{c}^{Y}+\tau_{ij}^{X_aX_b}+\tau_{ic}^{X_aY}+\tau_{jc}^{X_bY}+\tau_{ijc}^{X_aX_bY}
\end{equation}
and the homogeneous association model
\begin{equation}\label{eq:boost-homo}
\log{\mu_{ijc}}=\tau+\tau_{i}^{X_a}+\tau_{j}^{X_b}+\tau_{c}^{Y}+\tau_{ij}^{X_aX_b}+\tau_{ic}^{X_aY}+\tau_{jc}^{X_bY}
\end{equation}
is estimated in order to measure the effect size of the interaction term $\tau_{ijc}^{X_pX_qY}$, where $X_a$ and $X_b$ represent the genotype variable at loci $a$ and $b$, respectively, $Y$ represents the phenotype variable. The values $i$, $j$ and $c$ are the instances of $X_a$ and $X_b$ and $Y$, respectively. Here, the $\mu_{ijc}$ is the expected count of individuals in the cell $\{X_a=i,X_b=j,Y=c\}$ of a contingency table.

The output statistic of BOOST method, which is the LRT between the saturated model (Equation~\ref{eq:boost-saturated}) and the homogeneous association model (Equation~\ref{eq:boost-homo}), is calculated as
\begin{equation}\label{eq:boost-stat}
S=2n(\sum_{i,j,c}[\hat\pi_{ijc}\log{\frac{\hat\pi_{ijc}}{\hat{p}_{ijc}}}]+\log{\sum_{i,j,c}{\hat{p}_{ijc}}})
\end{equation}
where
\begin{align*}
\hat\pi_{ijc}=&\frac{n^c_{ij}}{n}\\
\hat{p}_{ijc}=&\frac{\hat\mu_{ijc}}{n}
\end{align*}
and $\hat\mu_{ijc}$ is the estimate of the expected count $\mu_{ijc}$.

There are two approaches to calculating $\hat\mu_{ijc}$. The first one is a rough one-step estimate, which is used in the screening stage of BOOST, while the second one is an iterative estimate with higher precision, which is used in the testing stage of BOOST. We now describe the two estimates in Algorithm~\ref{algo:BOOST-screen} and~\ref{algo:BOOST-test}, respectively.
\IncMargin{1em}
\begin{algorithm}
\SetKwData{Left}{left}\SetKwData{This}{this}\SetKwData{Up}{up}
\SetKwFunction{Union}{Union}\SetKwFunction{FindCompress}{FindCompress}
\SetKwInOut{Input}{input}\SetKwInOut{Output}{output}
\Input{Observed contingency table with cells $n^c_{ij}$}
\Output{$\hat\mu_{ijc}$}
\lForAll{$j$,$i$}{
    $a_{ji}\leftarrow \frac{n_{ij}}{n_{*j}}$
}
\lForAll{$c$,$j$}{
    $b_{cj}\leftarrow \frac{n^c_{*j}}{n_c}$
}
\lForAll{$i$,$c$}{
    $c_{ic}\leftarrow \frac{n^c_{i*}}{n_{i*}}$
}
\lForAll{$i$,$j$,$c$}{
    $\hat\mu_{ijc}\leftarrow a_{ji}\times b_{cj}\times c_{ic}$
}
\caption{One-step estimate of $\hat\mu_{ijc}$ in the screening stage of BOOST.}\label{algo:BOOST-screen}
\end{algorithm}\DecMargin{1em}

\IncMargin{1em}
\begin{algorithm}
\SetKwData{Left}{left}\SetKwData{This}{this}\SetKwData{Up}{up}
\SetKwFunction{Union}{Union}\SetKwFunction{FindCompress}{FindCompress}
\SetKwInOut{Input}{input}\SetKwInOut{Output}{output}
\Input{Observed contingency table with cells $n^c_{ij}$}
\Output{$\hat\mu_{ijc}$}
$e\leftarrow 1.0$
\lForAll{$i$,$j$,$c$}{
    $\hat\mu_{ijc}\leftarrow 1$
}
\While{$e>0.001$}{
    \lForAll{$i$,$j$,$c$}{
        $\hat\mu^{'}_{ijc}\leftarrow \hat\mu_{ijc}$
    }

    \lForAll{$i$,$j$}{
        $a_{ij}\leftarrow \sum_{c}{\hat\mu_{ijc}}$
    }
    \lForAll{$i$,$j$,$c$}{
        $\hat\mu_{ijc}\leftarrow \frac{\hat\mu_{ijc}\times n_{ij}}{a_{ij}}$
    }

    \lForAll{$i$,$c$}{
        $b_{ic}\leftarrow \sum_{j}{\hat\mu_{ijc}}$
    }
    \lForAll{$i$,$j$,$c$}{
        $\hat\mu_{ijc}\leftarrow \frac{\hat\mu_{ijc}\times n^c_{i*}}{b_{ic}}$
    }

    \lForAll{$j$,$c$}{
        $c_{jc}\leftarrow \sum_{i}{\hat\mu_{ijc}}$
    }
    \lForAll{$i$,$j$,$c$}{
        $\hat\mu_{ijc}\leftarrow \frac{\hat\mu_{ijc}\times n^c_{*j}}{c_{jc}}$
    }

    $e\leftarrow 0.0$
    \lForAll{$i$,$j$,$c$}{
        $e\leftarrow e+|\hat\mu_{ijc}-\hat\mu^{'}_{ijc}|$
    }
}
\caption{Iterative estimate of $\hat\mu{ijc}$ in the testing stage of BOOST method.}\label{algo:BOOST-test}
\end{algorithm}\DecMargin{1em}

Using the above estimates, the statistic $S$ in Equation~\ref{eq:boost-stat} can be calculated. Subsequently, the significance $p$-value can be assigned based on that $S$ follows approximately $\chi^2$ distribution with 4 degree of freedom.


